package az.atl.demosecurity3.scheduler;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@Component
@Slf4j
public class TestScheduler {
//    private static final String DB_URL = "jdbc:postgresql://localhost:5432/atl_academy_security";
//    private static final String DB_USERNAME = "postgres";
//    private static final String DB_PASSWORD = "12345";
//    private static final String TABLE_NAME = "t_user";
//    private static final String COLUMN_NAME = "phone_number";
//    private static final String COLUMN_TYPE = "VARCHAR(255)";
//
//    @SneakyThrows
//    @Scheduled(fixedRate = 1000 * 60 * 5)
//    private static void createColumn() {
//        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD)) {
//            Statement statement = connection.createStatement();
//            String sql = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN IF NOT EXISTS " + COLUMN_NAME + " " + COLUMN_TYPE;
//            statement.executeUpdate(sql);
//            System.out.println("Column " + COLUMN_NAME + " created successfully.");
//        } catch (SQLException e) {
//            System.err.println("Error creating table: " + e.getMessage());
//        }
//    }
//     @SneakyThrows
//    @Scheduled(initialDelay = 15000,fixedRate = 1000 * 60 * 4 )
//    private static void dropColumn() {
//        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD)) {
//            Statement statement = connection.createStatement();
//            String sql = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN " + COLUMN_NAME;
//            statement.executeUpdate(sql);
//            System.out.println("Column " + COLUMN_NAME + " dropped successfully.");
//        } catch (SQLException e) {
//            System.err.println("Error dropping table: " + e.getMessage());
//        }
//    }
}
