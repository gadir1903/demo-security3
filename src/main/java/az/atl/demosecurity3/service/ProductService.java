package az.atl.demosecurity3.service;

import az.atl.demosecurity3.client.ProductClient;
import az.atl.demosecurity3.model.client.ProductDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductClient productClient;

    public List<ProductDto> getAllProducts(){
        return productClient.getAllProducts();
    }
}
