package az.atl.demosecurity3.service;

import az.atl.demosecurity3.dao.entity.User;
import az.atl.demosecurity3.dao.repository.UserRepo;
import az.atl.demosecurity3.model.AuthenticationRequest;
import az.atl.demosecurity3.model.AuthenticationResponse;
import az.atl.demosecurity3.model.RegisterRequest;
import az.atl.demosecurity3.model.RegisterResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public record AuthenticationService(UserRepo repo,
                                    PasswordEncoder passwordEncoder,
                                    JwtService jwtService,
                                    AuthenticationManager authenticationManager) {

    public RegisterResponse register(RegisterRequest request){
        var exist = repo.findByEmail(request.getEmail()).isPresent();
        if (exist){
            throw new RuntimeException("Email already exist!");
        }
        var user = User.builder()
                .fullName(request.getFullName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole())
                .phoneNumber((request.getPhoneNumber()))
                .build();
        var userEntity = repo.save(user);
        return RegisterResponse.buildRegisterDto(userEntity);

//        return "Full Name: " + request.getFullName().toUpperCase() + '\n' +
//                "Email: " + request.getEmail() + '\n' +
//                "Password: " + request.getPassword() + '\n' +
//                "Role: " + request.getRole();
    }
    public AuthenticationResponse authenticate(AuthenticationRequest request){
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        var user = repo.findByEmail(request.getEmail())
                .orElseThrow(() -> new RuntimeException("User not found"));
        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
}
