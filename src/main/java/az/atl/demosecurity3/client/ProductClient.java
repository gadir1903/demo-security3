package az.atl.demosecurity3.client;

import az.atl.demosecurity3.model.client.ProductDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(value = "productClient" , url = "http://localhost:8083/product")
public interface ProductClient {
    @RequestMapping(method = RequestMethod.GET , value = "/allProducts")
    List<ProductDto>getAllProducts();
}
