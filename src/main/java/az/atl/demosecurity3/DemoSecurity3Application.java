package az.atl.demosecurity3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling

@EnableFeignClients
@ImportAutoConfiguration({FeignAutoConfiguration.class})
public class DemoSecurity3Application {
	public static void main(String[] args) {
		SpringApplication.run(DemoSecurity3Application.class, args);
	}

}
